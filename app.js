var http = require('http');

// create a server
http.createServer(function (req, res) {
    res.writeHead(200, {'Content-Type': 'text/html'});
    
    var url = req.url;
    
    if(url ==='/html'){
        res.write('<h1>Hello World!<h1>');
        res.end();
    }else{
       res.write('Hello World!');
       res.end();
    }
}).listen(3000, function(){
    console.log("server start at port 3000");
});